#Errors
##Can't Install Any Thing

```shell
sudo rm /var/lib/apt/lists/lock
sudo rm /var/cache/apt/archives/lock
sudo rm /var/lib/dpkg/lock
```


#INSTALL LAMP

### APACHE2
```shell
sudo apt-get update
sudo apt-get install apache2
```
### Nginx
```shell
sudo apt update
sudo apt install nginx
```

```shell
sudo ufw app list
sudo ufw allow 'Nginx HTTP'
sudo ufw status


```
```shell
systemctl status nginx
sudo systemctl enable nginx
```
```shell
sudo mkdir -p /var/www/nginx/html
sudo chown -R $USER:$USER /var/www/nginx/html
sudo chmod -R 755 /var/www/nginx
nano /var/www/nginx/html/index.html
```
```shell
<html>
    <head>
        <title>Welcome to nginx!</title>
    </head>
    <body>
        <h1>Success!  The nginx server block is working!</h1>
    </body>
</html>
```
```shell
sudo nano /etc/nginx/sites-available/nginx
```
```shell

server {
        listen 80;
        listen [::]:80;

        root /var/www/nginx/html;
        index index.html index.htm index.nginx-debian.html;

        server_name nginx www.nginx;

        location / {
                try_files $uri $uri/ =404;
        }
}
```
```shell
sudo ln -s /etc/nginx/sites-available/nginx /etc/nginx/sites-enabled/
sudo nano /etc/nginx/nginx.conf

```
```shell
...
http {
    ...
    server_names_hash_bucket_size 64;
    ...
}
...
```
```shell
sudo nginx -t
sudo systemctl restart nginx
```

### Elasticsearch

```shell
sudo apt update
sudo apt install apt-transport-https ca-certificates wget
wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
sudo sh -c 'echo "deb https://artifacts.elastic.co/packages/7.x/apt stable main" > /etc/apt/sources.list.d/elastic-7.x.list'
sudo apt install elasticsearch
sudo systemctl enable --now elasticsearch.service
curl -X GET "localhost:9200/"
```
### ElasticSearch Remote Access
```shell
sudo ufw allow proto tcp from 192.168.121.0/24 to any port 6379
sudo nano /etc/elasticsearch/elasticsearch.yml
//change
network.host: 0.0.0.0
sudo systemctl restart elasticsearch
```

### mcrypt

```shell
sudo apt-get -y install gcc make autoconf libc-dev pkg-config
sudo apt-get -y install libmcrypt-dev
sudo pecl install mcrypt-1.0.1
sudo bash -c "echo extension=/usr/lib/php/20170718/mcrypt.so > /etc/php/7.2/cli/conf.d/mcrypt.ini"
sudo bash -c "echo extension=/usr/lib/php/20170718/mcrypt.so > /etc/php/7.2/apache2/conf.d/mcrypt.ini"
sudo service apache2 restart
```

### RabbitMQ
```shell
curl -s https://packagecloud.io/install/repositories/rabbitmq/rabbitmq-server/script.deb.sh | sudo bash
sudo apt update
sudo apt install rabbitmq-server
sudo systemctl status  rabbitmq-server.service
sudo systemctl enable rabbitmq-server
sudo rabbitmq-plugins enable rabbitmq_management
sudo ss -tunelp | grep 15672
sudo ufw allow proto tcp from any to any port 5672,15672
By default, the guest user exists and can connect only from localhost. You can login with this user locally with the password “guest”
```
Install by Docker
```
docker run -d --hostname notchbot --name some-rabbit -p 15672:15672 -p 5672:5672 rabbitmq:3-management
```
### RabbitMQ Users
```shell

sudo rabbitmqctl add_user admin StrongPassword
sudo rabbitmqctl set_user_tags admin administrator

Delete User:
rabbitmqctl delete_user user

Change User Password:
rabbitmqctl change_password user strongpassword

Create new Virtualhost:
rabbitmqctl add_vhost /my_vhost

List available Virtualhosts:
rabbitmqctl list_vhosts

Delete a virtualhost:
rabbitmqctl delete_vhost /myvhost
Grant user permissions for vhost:

rabbitmqctl set_permissions -p /myvhost user ".*" ".*" ".*"

List vhost permissions:
rabbitmqctl list_permissions -p /myvhost

To list user permissions:
rabbitmqctl list_user_permissions user

Delete user permissions:
rabbitmqctl clear_permissions -p /myvhost user

```
## PHP7.4

```shell
sudo apt-get install python-software-properties
sudo add-apt-repository ppa:ondrej/php
sudo apt-get update
sudo apt-get install -y php7.4
sudo apt-cache search php7-*
sudo apt-get install php7.4-mysql php7.4-curl php7.4-json php7.4-cgi php7.4-xsl php7.4-mbstring
```

## MYSQL5.7

```shell
sudo apt update
sudo apt install mysql-server
sudo mysql_secure_installation
sudo mysql
ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'root'; #mysql
FLUSH PRIVILEGES; #mysql
CREATE USER 'phpmyadmin'@'localhost' IDENTIFIED BY 'root';
GRANT ALL PRIVILEGES ON *.* TO 'phpmyadmin'@'localhost' WITH GRANT OPTION;
SELECT user,authentication_string,plugin,host FROM mysql.user; #mysql
sudo service mysql restart
sudo cp /usr/share/phpmyadmin/libraries/sql.lib.php /usr/share/phpmyadmin/libraries/sql.lib.php.bak
sudo gedit /usr/share/phpmyadmin/libraries/sql.lib.php
exit
```


> Press Ctrl + W And Search For (Count($Analyzed_sql_results['Select_expr'] == 1)
Replace It With ((Count($Analyzed_sql_results['Select_expr']) == 1)

## ALLOW HTTACCESS

```shell
sudo a2enmod rewrite
sudo nano /etc/apache2/apache2.conf
```

```rst
<Directory /var/www/>
    Options Indexes FollowSymLinks
    AllowOverride All
    Require all granted
</Directory>
```

```shell
sudo systemctl restart apache2

```
## PHPMYADMIN

```shell
sudo apt-get install phpmyadmin
sudo gedit /etc/apache2/apache2.conf
```
> 
add this to end of the file

> Include /etc/phpmyadmin/apache.conf

> at the end

```shell
sudo service apache2 restart
```

## ADD NEW VIRSUAL HOST

###### ##### #### ###  /etc/apache2/sites-available/work.dev.conf contains following lines

```rst
<VirtualHost *:80>

        ServerName work.com #must .com
        
        DocumentRoot /var/www/html/work/public
        
    ErrorLog ${APACHE_LOG_DIR}/error.log
    
        CustomLog ${APACHE_LOG_DIR}/access.log combined
        
        
    <Directory /var/www/html/work/public>
    
            AllowOverride All
            
            Require all granted
            
        </Directory>
        
</VirtualHost>

```
*add to hosts*

```shell
sudo gedit /etc/hosts

```
127.0.0.1 work.com #url work.com

enable site

```shell
sudo a2ensite work.com
service apache2 reload
```

## GIT


```shell
sudo apt-get install git

```
## zip

```shell
sudo apt-get install zip

```
## COMPOSER

```shell
sudo apt install curl

sudo apt install composer

curl -sS https://getcomposer.org/installer | php

sudo mv composer.phar /usr/local/bin/composer
```

## NODEJS 12

```shell
sudo apt-get install python-software-properties

curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -

sudo apt-get install nodejs
```

## MONGODB

```shell
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 2930ADAE8CAF5059EE73BB4B58712A2291FA4AD5

echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.6 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.6.list

sudo apt-get update

sudo apt-get install -y mongodb-org

sudo service mongod start

sudo service mongodb start

sudo apt-get install php-dev

sudo pecl install memcache

sudo pecl install mongodb

Add mongodb.so to php.ini

sudo apt-get install php-mongodb

mongod --config /etc/mongod.conf

```
https://nosqlbooster.com/downloads

## MONGODB 4.2
```shell
sudo apt-get install gnupg
wget -qO - https://www.mongodb.org/static/pgp/server-4.2.asc | sudo apt-key add -
```
#### then 
```shell
echo "deb [ arch=amd64 ] https://repo.mongodb.org/apt/ubuntu bionic/mongodb-org/4.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.2.list

sudo apt-get update

sudo apt-get install mongodb-org

sudo service mongod start

sudo service mongod status

Add mongodb.so to php.ini

mongo

mongosh

```
## uninstall mongo
```shell

sudo service mongod stop

sudo apt-get purge mongodb-org*

sudo rm -r /var/log/mongodb

sudo rm -r /var/lib/mongodb

sudo apt autoremove

```
## to run remote mongo outside server 
```shell

sudo mongod --bind_ip_all

```
if you want open port also then change this by this 

```shell
sudo nano /etc/mongod.conf
```
change 

bind ip from 127.0.0.1 to 0.0.0.0 or
bind ip add new ip remote 127.0.0.1,127.1.1.1,127.34.34.5

```shell
sudo service mongod restart
sudo service mongod status
```

## show ports run 
```shell

sudo lsof -iTCP -sTCP:LISTEN -n -P

```
to kill port 

```shell

sudo kill <mongo_command_pid>

```
## install php driver

```shell

sudo pecl install mongodb-1.6.1
sudo service apache restart
```

## install SSL to apache

create Private key 'private.key' and 'certificate.crt' and 'ca_bundle.crt'
then 
```shell
sudo cp /etc/apache2/sites-available/000-default.conf /etc/apache2/sites-available/default.backup.conf
sudo a2enmod ssl
sudo service apache restart
sudo nano /etc/apache2/sites-available/000-default.conf
```
this is VirtualHost
```shell
<VirtualHost domain.com:443>
        ServerName domain.com
        ServerAdmin webmaster@localhost
        ServerAlias *.domain.com
 DocumentRoot /var/www/html/domain.com/front/build
        ErrorLog ${APACHE_LOG_DIR}/error.log
        CustomLog ${APACHE_LOG_DIR}/access.log combined#
        SSLEngine On
        SSLCertificateFile      /certificate.crt
        SSLCertificateKeyFile /private.key
        SSLCACertificateFile /ca_bundle.crt
</VirtualHost>

<VirtualHost domain.com:80>
ServerName domain.com
        DocumentRoot /var/www/html/domain.com/front/build
  RewriteEngine On
    RewriteCond %{HTTPS} off
    RewriteRule (.*) https://%{HTTP_HOST}%{REQUEST_URI}
</VirtualHost>


<VirtualHost domain.com:81>
    ServerAdmin webmaster@localhost
    ServerName domain.com
    ServerAlias *.domain.com
 DocumentRoot /var/www/html/domain.com/api/public
RewriteEngine On
    RewriteCond %{HTTPS} off
    RewriteRule (.*) https://%{HTTP_HOST}%{REQUEST_URI}
    ErrorLog /var/www/html/domain.com/api/error.log
    CustomLog /var/www/html/domain.com/api/access.log combined
    SSLEngine On
    SSLCertificateFile      /certificate.crt
    SSLCertificateKeyFile /private.key
    SSLCACertificateFile /ca_bundle.crt
</VirtualHost>

```
run 
```shell
sudo service apache restart
```
add Port to listen Apache2

```shell
sudo nano /etc/apache2/ports.conf
```
add ports
```shell
Listen 80
Listen 81
Listen 85
<IfModule ssl_module>
        Listen 443
</IfModule>
<IfModule mod_gnutls.c>
        Listen 443
</IfModule>
```
run 
```shell
sudo service apache restart
```
## REDIS

```shell
sudo add-apt-repository ppa:chris-lea/redis-server

sudo apt-get update

sudo apt-get install redis-server

sudo service redis-server start
```

#####   Laravel Redis ==>

```shell
 composer require predis/predis

``` 
```rst
CACHE_DRIVER=redis

SESSION_DRIVER=redis

CACHE_DRIVER=file

SESSION_DRIVER=file

QUEUE_DRIVER=sync

REDIS_HOST=127.0.0.1

REDIS_PASSWORD=null

REDIS_PORT=6379
```
Add Password to Redis
```
sudo nano /etc/redis/redis.conf 
find and uncomment line # requirepass foobared, then restart server

now you password is foobared
```
OR 
using redis-cli:
```
root@server:~# redis-cli 
127.0.0.1:6379> CONFIG SET requirepass secret_password
OK
```
this will set password temporarily (until redis or server restart)

test password:
```
root@server:~# redis-cli 
127.0.0.1:6379> AUTH secret_password
OK
```
## REDIS Manager

https://github.com/uglide/RedisDesktopManager/releases

```shell
sudo apt-get install gdebi
sudo snap install redis-desktop-manager

```
## BOWER

```shell
sudo npm install bower -g

```
## WINE

* If your system is 64 bit, enable 32 bit architecture!

```shell
sudo dpkg --add-architecture i386
```
```shell
wget -nc https://dl.winehq.org/wine-builds/Release.key

sudo apt-key add Release.key

sudo apt-add-repository 'deb http://dl.winehq.org/wine-builds/ubuntu/ xenial main'

sudo apt-get update

sudo apt-get install --install-recommends winehq-devel

winecfg
```

## RAR

```shell
sudo apt-get install unrar 

```
## Angularjs

```shell
npm config set unsafe-perm true

sudo npm install -g @angular/cli

ng new my-project-name
```

https://github.com/angular/angular-cli/wiki

##Monitor

## gnome-system-monitor

```shell
sudo apt-get install gnome-system-monitor

gnome-system-monitor
```

## Docker
```shell

sudo apt-get update

sudo apt-get install apt-transport-https ca-certificates  curl  software-properties-common

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs)  stable"

sudo apt-get update

sudo apt-get install docker-ce

sudo apt install docker-compose

sudo usermod -aG docker engshokry

sudo chmod -R 777 /var/lib/docker/

sudo mkdir /var/lib/docker/tmp    ==> if not create

sudo systemctl start docker

```
## SWITCH PHP

```shell
sudo a2dismod php7.0 && sudo a2enmod php5.6 && sudo service apache2 restart

sudo ln -sfn /usr/bin/php5.6 /etc/alternatives/php
```

## Gdebi

```shell
sudo apt install gdebi

```
## Sendmail

```shell
sudo apt install sendmail
```

which sendmail

```shell
sudo gedit /etc/ssmtp/ssmtp.conf

```
##Config Sendmail

mailhub=smtp.gmail.com:587 ######YES 465 NO 587

rewriteDomain=gmail.com

AuthUser=sms.shokry.mohamed@gmail.com

AuthPass=password

UseSTARTTLS=YES

###### FromLineOverride=YES

###### UseTLS=YES # YES 465

## Keyboard

```shell
sudo apt-get install gnome-tweaks

```
Then Open Gnome Tweaks (Gnome-Tweaks).

Select Keyboard & Mouse Tab

Click Additional Layout Options Button

Expand Switching To Another Layout

Select Ctrl + Shift Here

See Screenshot Below:

https://i.stack.imgur.com/6eUtV.png

##create Short Icon to Your program

```shell
Installing gnome-panel

 sudo apt-get install --no-install-recommends gnome-panel
```
To create launcher

```shell
sudo gnome-desktop-item-edit /usr/share/applications/ --create-new

```
This will open up a "Create Launcher" window

```rst
Type: Application

Name: PhpStorm

Command: /bin/bash path_to/phpstorm.sh

Comment: Any Comment
```

This will create a launcher file in /usr/share/applications directory. Now double click and open the file.


## Install Vega Device

If you want install automatic 
```shell
sudo add-apt-repository ppa:oibaf/graphics-drivers
ubuntu-drivers devices
sudo ubuntu-drivers autoinstall
```

First What is your devices (amd)

```shell
lspci|grep VGA  // Download package on here https://www.amd.com/en/support
sudo ./amdgpu-pro-install // reset your pc after this install
dpkg -l amdgpu-pro  // if install

```

## Python3
```shell
sudo add-apt-repository ppa:deadsnakes/ppa
sudo add-apt update
sudo apt-get install python3.7
sudo apt-get install python3-pip // pip install
```
To make python3 use the new installed python 3.6 instead of the default 3.7 release, run following 2 commands:
```shell
sudo update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.5

sudo update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.7
```
Finally switch between the two python versions for python3 via command:
```shell
sudo update-alternatives --config python3
```
After selecting version 3.7:
```shell
python3 -V
```

UPDATE: due to this bug, gnome-terminal won�t launch after step 3, a workaround is running following commands to recreate the symlink:
```shell
sudo rm /usr/bin/python3
sudo ln -s python3.5 /usr/bin/python3
```

## python change version
From the comment:
```shell
sudo update-alternatives --config python
```
Will show you an error:
```shell
update-alternatives: error: no alternatives for python3 
```
You need to update your update-alternatives , then you will be able to set your default python version.
```shell
sudo update-alternatives --install /usr/bin/python python /usr/bin/python3.4 1
sudo update-alternatives --install /usr/bin/python python /usr/bin/python3.6 2
```
Then run :
```shell
sudo update-alternatives --config python
```
Set python3.6 as default.


## python install connect mysql
```shell
sudo apt-get install python-pip
pip search mysql-connector | grep --color mysql-connector-python
pip install mysql-connector-python-rf
```


##PHPstorm Active

add line in hosts
```shell
sudo gedit /etc/hosts
```

0.0.0.0 account.jetbrains.com
http://webstorm.autoseasy.cn/xixi License Server

### File Upload Git for Local git.sh File Script
```
###!/bin/sh
cd /C/wamp64/www/kitchen
git checkout dev
git add .
git commit -am "`date`"
git pull
git add .
git commit -am "merge `date`"
git pull
git push
echo Press Enter...
read

```

### File Download Git for server server.bat File Script
```
start putty.exe -ssh 120.13.14.86 -l dfer2pcynv3 -pw 7AdssvaX@cyds -m "D:\server_odes.txt"
pause
```
## server_odes.txt
```
cd public_html/new_kitchen/
git pull  https://username:password@gitlab.com/vlinkteam/kitchen.git master
echo enter
avahi-daemon ... ; /bin/bash
```

## spatie/laravel-permission
```
php artisan cache:forget spatie.permission.cache 
```
then 
```
php artisan cache:clear
```

## How to Create CRS File For SSL
By This tool 
https://www.digicert.com/easy-csr/openssl.htm
sure this filed is correct 
Common Name

## How To Creat Private Key File For SSL
openssl req –new –newkey rsa:2048 –nodes –keyout server.key –out server.csr
#### OR
openssl req -new -newkey rsa:2048 -nodes -out star_yourwhatsapp_com.csr -keyout star_yourwhatsapp_com.key -subj "/C=EG/ST=maadi/L=cairo/O=VictoryLink/OU=IT/CN=*.yourwhatsapp.com"
Check you not add Password to File

Link Check SSL
https://www.sslshopper.com/ssl-checker.html
lik check length
http://string-functions.com/length.aspx
## zoho Provider Mail
https://mail.zoho.com/

## Block ports
```
1,       // tcpmux
7,       // echo
9,       // discard
11,      // systat
13,      // daytime
15,      // netstat
17,      // qotd
19,      // chargen
20,      // ftp data
21,      // ftp access
22,      // ssh
23,      // telnet
25,      // smtp
37,      // time
42,      // name
43,      // nicname
53,      // domain
77,      // priv-rjs
79,      // finger
87,      // ttylink
95,      // supdup
101,     // hostriame
102,     // iso-tsap
103,     // gppitnp
104,     // acr-nema
109,     // pop2
110,     // pop3
111,     // sunrpc
113,     // auth
115,     // sftp
117,     // uucp-path
119,     // nntp
123,     // NTP
135,     // loc-srv /epmap
139,     // netbios
143,     // imap2
179,     // BGP
389,     // ldap
427,     // SLP (Also used by Apple Filing Protocol)
465,     // smtp+ssl
512,     // print / exec
513,     // login
514,     // shell
515,     // printer
526,     // tempo
530,     // courier
531,     // chat
532,     // netnews
540,     // uucp
548,     // AFP (Apple Filing Protocol)
556,     // remotefs
563,     // nntp+ssl
587,     // stmp?
601,     // ??
636,     // ldap+ssl
993,     // ldap+ssl
995,     // pop3+ssl
2049,    // nfs
3659,    // apple-sasl / PasswordServer
4045,    // lockd
6000,    // X11
6665,    // Alternate IRC [Apple addition]
6666,    // Alternate IRC [Apple addition]
6667,    // Standard IRC [Apple addition]
6668,    // Alternate IRC [Apple addition]
6669,    // Alternate IRC [Apple addition]
6697,    // IRC + TLS
```
## Add Ports
```
sudo nano /etc/apache2/ports.conf 
```
## Add port allow to firewall
```
 sudo ufw allow 91/tcp
 sudo ufw reload

```
## add user for mongodb

```

db.createUser(
  {
    user: "Admin",
    pwd: "VictoryLink@421", // or cleartext password
    roles: [ { role: "userAdminAnyDatabase", db: "admin" }]
  }
)

VictroyLink@421
mongo --port 27017  --authenticationDatabase "admin" -u "WhatsappBusinessAdmin" -p

```

## Fix cors
1- add to file config  
```
sudo nano /etc/apache2/apache2.conf
```

```
<IfModule mod_headers.c>
Header set Access-Control-Allow-Origin "*"
</IfModule>
```
Like
```
<Directory /var/www/>
        Options Indexes FollowSymLinks
        AllowOverride All
        Require all granted
<IfModule mod_headers.c>
Header set Access-Control-Allow-Origin "*"
</IfModule>

</Directory>
```
2- add package Cors to api system
 
3- In order to use Header directive in apache you have to load mod_header module. You can test if module is loaded or not by :-

apache2ctl -M | grep  headers_module

find / -name mod_headers.so

If it is loaded you will see something like :-

headers_module (shared)

/usr/lib/apache2/modules/mod_headers.so

If you see no output of find command than load that module directly in your apache conf file. Just append below line :-

LoadModule headers_module modules/mod_headers.so

Note :- mod_header is available as base module in apache. So you don't need to install it explicitly.

Issue following command :-
```
a2enmod headers
```
Restart web service
```
apache2ctl restart
```
## Fix Error Bad Request
Your browser sent a request that this server could not understand.
Reason: You're speaking plain HTTP to an SSL-enabled server port.
Instead use the HTTPS scheme to access this URL, please.


add code in file viruval 

```
 RewriteEngine On
    RewriteCond %{HTTPS} off
    RewriteRule (.*) https://%{HTTP_HOST}%{REQUEST_URI}
```
Like
```

<VirtualHost domain.com:443>
        ServerName domain.com
        ServerAdmin webmaster@localhost
        ServerAlias *.domain.com
        DocumentRoot /var/www/html/
        ErrorLog ${APACHE_LOG_DIR}/error.log
        CustomLog ${APACHE_LOG_DIR}/access.log combined#
        SSLEngine On
        SSLCertificateFile      /certificate.crt
        SSLCertificateKeyFile /private.key
        SSLCACertificateFile /ca_bundle.crt
</VirtualHost>

<VirtualHost domain.com:80>
ServerName domain.com
        DocumentRoot /var/www/html/
  RewriteEngine On
    RewriteCond %{HTTPS} off
    RewriteRule (.*) https://%{HTTP_HOST}%{REQUEST_URI}
</VirtualHost>


<VirtualHost notchbot.com:81>
    ServerAdmin webmaster@localhost
    ServerName domain.com
    ServerAlias *.domain.com
 DocumentRoot /var/www/html/public
RewriteEngine On
    RewriteCond %{HTTPS} off
    RewriteRule (.*) https://%{HTTP_HOST}%{REQUEST_URI}
    ErrorLog /var/www/html/domain.com/api/error.log
    CustomLog /var/www/html/domain.com/api/access.log combined
    SSLEngine On
    SSLCertificateFile      /certificate.crt
    SSLCertificateKeyFile /private.key
    SSLCACertificateFile /ca_bundle.crt
</VirtualHost>


```


## SSl Content-Length
get from this url
```
http://string-functions.com/length.aspx
```
## API 
run queue on background server with with this command 
```
cd /var/www/html/whatsapp-business/
pm2 start laravel-queue-worker.yml
```

## ReactJs Front
TO build Project from src folder to Build Folder
run this command local and upload with git
```
cd path/to/folder/project
npm run build
```


## Socket Nodejs
To run socket with this command
```
cd /var/www/html/serversocket/
npm start
```

to run socket on background 
```
cd /var/www/html/serversocket/
pm2 start npm -- start
```



## Docker
to run Docker with this commands
```
cd ~/biz/
export WA_API_VERSION="2.27.12"
docker-compose up -d
```

to restart Docker
```
cd ~/biz/
docker restart $(docker ps -q)
export WA_API_VERSION="2.27.12"
docker-compose up -d
```

to get logs for docker last 500 error
```
docker logs biz_wacore_1 --tail=500

docker-compose -f prod-docker-compose.yml logs > debug_output.txt

```
To remove dokcer
```
https://linuxize.com/post/how-to-remove-docker-images-containers-volumes-and-networks/
```
To Status 
```
docker stats
```

## PM2
to get Logs
```
pm2 logs laravel-queue-worker
```
add two npm server to pm2
```
pm2 start --name=APP_NAME npm -- start
pm2 start --name=APP_NAME --no-autorestart npm -- start
``` 
## MongoDB Backup

```
sudo mongodump --out=/var/www/html/backup12-11/ --port=27017 -u="Admin"
```
## MongoDB Restore

```
sudo mongorestore /backup12-11/ --port=22023 -u="Admin"
```

## Email Regex
```
regex:/^(?-i)[a-z]+[a-z0-9._]+@[a-z0-9.]+\.[a-z]{2,}$/i|email
```

### Change Apache document root folder to secondary hard drive
You'll have to edit apache2.conf and 000-default.conf to change the document root of apache.

The Apache server is installed on /var/www/html.This is the default root directory of apache.

Either change the root directory of Apache or move the project to /var/www/html.

To change Apache's root directory, run:
```
 cd /etc/apache2/sites-available
```
Then open the 000-default.conf file using the command:
```
 nano 000-default.conf
```
Edit the DocumentRoot option:

 DocumentRoot /path/to/my/project
Then restart the apache server:
```
 sudo service apache2 restart
```
If you get Forbidden You don't have permission to access / on this server after changing the root of apache then do follow these steps

Find the apache2.conf located in /etc/apache2 and open it using:
```
 nano apache2.conf
```
Use Ctrl+W and search for Directory (It should be in line 153)

It should look like this
```
 <Directory />
     Options Indexes FollowSymLinks
     AllowOverride All
     Require all denied
 </Directory>
 ```
Change it to
```
 <Directory />
     Options Indexes FollowSymLinks Includes ExecCGI
     AllowOverride All
     Require all granted
 </Directory>
 ```
Restart apache
```
 sudo service apache2 restart
chown www-data /media;
chown www-data /media/MNT/;
chown www-data /media/MNT/DISK;
chown www-data /media/MNT/DISK/www-root;
```
### connect ssh
```
ssh adminstrator@104.217.252.114 -p22

```


### Run PhpStorm from terminal and create launcher in Ubuntu

```
sudo ln -s /opt/PhpStorm-121.390/bin/phpstorm.sh /usr/bin/phpstorm
```
In the above code /opt/PhpStorm-121.390/bin/phpstorm.sh is your PhpStorm’s phpstorm.sh executable file location and phpstorm word from usr/bin/phpstorm this is the assigned command to run PHPStorm IDE from your terminal.

In the above command, change your directory as per your downloaded version. And /usr/bin/phpstorm you can make it however you want. If you want to run PhpStorm as another name like yourname. You can create that symbolic link to /usr/bin/yourname.

OK, now you have the binary located in bin directory. You can now open PhpStorm from terminal with the following command.
```
$ phpstorm
```
Run PhpStorm as sudo
Everything is same. Just add sudo before the command. So the final command to run open PhpStorm from terminal with sudo privilege is –
```
sudo phpstorm
```
Run and open PhpStorm from terminal in Ubuntu
Create PhpStorm Launcher in Ubuntu
Sometimes we don’t feel OK to run anything from terminal because of wrong command or we forget some useful commands. In that case specially if we use Ubuntu. Then we can directly create launcher icon of PhpStorm so like other program, we can just run PhpStorm by clicking on the icon. Sounds cool!

Now to create launcher in Ubuntu Unity we just need to create phpstorm.desktop file in /usr/share/applications with the following snippet :

Create phpstorm.desktop file by typing the following command :
```
sudo gedit /usr/share/applications/phpstorm.desktop
```
Then write the following code in newly created blank text file.
```
[Desktop Entry]
Version=5.0.4
Name=JetBrains PhpStorm
# Only KDE 4 seems to use GenericName, so we reuse the KDE strings.
# From Ubuntu's language-pack-kde-XX-base packages, version 9.04-20090413.
GenericName=Text Editor

Exec=phpstorm
Terminal=false
Icon=/opt/PhpStorm-121.390/bin/webide.png
Type=Application
Categories=TextEditor;IDE;Development
X-Ayatana-Desktop-Shortcuts=NewWindow

[NewWindow Shortcut Group]
Name=New Window
Exec=phpstorm
TargetEnvironment=Unity
```
Save this file and restart your terminal. 

Example for EXE
```
[Desktop Entry]
Name=Navicat Premium 12
Exec=env WINEPREFIX="/home/shokry/.wine" /opt/wine-staging/bin/wine C:\\\\windows\\\\command\\\\start.exe /Unix /home/shokry/.wine/dosdevices/c:/users/Public/Desktop/Navicat\\ Premium\\ 12.lnk
Type=Application
StartupNotify=true
Path=/home/shokry/.wine/dosdevices/c:/Program Files/PremiumSoft/Navicat Premium 12
Icon=8F8C_navicat.0
StartupWMClass=navicat.exe
```

Fixed Cors Add In File apache.cong 
```
Header set Access-Control-Allow-Origin "*"
Header set Access-Control-Allow-Methods "GET, POST, OPTIONS, PUT, DELETE"
Header set Access-Control-Allow-Headers "Access-Control-Allow-Headers,Access-Control-Allow-Origin,Access-Control-Allow-Methods,X-Accept-Charset,X-Accept,Content-Type,A>

OR

Header set Access-Control-Allow-Origin "*"
Header set Access-Control-Allow-Methods "*"
Header set Access-Control-Allow-Headers "*">
```

OR

```
<?php

namespace App\Http\Middleware;

use Closure;

class Cors
{
    public function handle($request, Closure $next)
    {
        header("Access-Control-Allow-Origin: *");

        $headers = [
            'Access-Control-Allow-Methods' => 'POST, GET, OPTIONS, PUT, DELETE',
            'Access-Control-Allow-Headers' => 'Content-Type, X-Auth-Token, Origin, Authorization'
        ];
        if ($request->getMethod() == "OPTIONS") {
            return response('OK')
                ->withHeaders($headers);
        }

        $response = $next($request);
        foreach ($headers as $key => $value)
            $response->header($key, $value);
        return $response;
    }
}


```

Open large file

```
split -b 53750k <your-file>

cat xa* > <your-file>
```
Show last line in file 
```
tail -n 3 laravel.log
```

sudo mongorestore 27Jul2022/ --nsInclude="database_agm.interactions" --port=00 -u="Admin"


### webpanel
centos web panel
webmin

### connect wpa 2.4 G.H
```
nm-connection-editor
```
Search Arabic in mysql and mongo
```
function generate_pattern($search_string)
{
    $patterns     = array("/(ا|إ|أ|آ)/", "/(ه|ة)/", "/(ي|ئ)/", "/(و|ؤ)/");
    $replacements = array("[ا|إ|أ|آ]", "[ه|ة]", "[ي|ئ]", "[و|ؤ]");
    return preg_replace($patterns, $replacements, $search_string);
}
        $title = generate_pattern(request()->title);
in mongo 

            $q->whereRaw(['title_ar' => ['$regex' => $title]])->orwhereRaw(['title_en' => ['$regex' => $title]])->orwhere('event_id', $title);
in sql
            $q->where("title", 'REGEXP', $title);
```
Forticlient 7 lincenacne
openssl s_client -connect 41.33.137.138:8123
